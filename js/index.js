$(document).ready(() => {
    $("nav-bar").componentHtml("nav_bar", [{ name: "UNIFOR" },]);
    $("main").componentHtml("main_section", [{}]);
    $("contact").componentHtml("contact", [{}]);
    $("form-group-contact").componentHtml("form_input_field", [
        {id: "username", icon: "fa fa-user", placeholder: "Username"},
        {id: "email", icon: "fa fa-at", placeholder: "Email"},
        {id: "phone", icon: "fa fa-phone", placeholder: "Phone Number"}
        ]);
    $("places").componentHtml("place_img_right", [{img_src: "img/CC_unifor.jpg"}])
    $("footer").componentHtml("footer", [{}]);
    generateListPlaces()
});

function generateListPlaces(){
    let places = [ "Centro de Convivencia", "NAMI", "NATI", "Ginasio" ];
    let imgs = [ "CC_unifor.jpg", "NAMI.jpg", "NATI.png", "GINASIO_UNIFOR.jpeg" ];
    let html =  ``;
    $.each(places, function(key, value){
        console.log(key % 2);
        // if( key % 2 == 0){
            html += getComponent("place_img_card", [{img_src: "img/"+imgs[key], place_name: places[key]}])
        // }else{
        //     html += getComponent("place_img_left", [{img_src: "img/"+imgs[key]}])
        // }
    })
    $(".places").html(html);
}

function myMap() {
    var mapProp= {
        center:new google.maps.LatLng(-3.7687753,-38.478061),
        zoom:16,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    var marker = new google.maps.Marker({position: mapProp, map: map});
}