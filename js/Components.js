const Components = {
    nav_bar: ({name}) => `
         <div class="navbar navbar-dark bg-dark shadow-sm">
            <div class="container d-flex justify-content-between">
              <a href="#" class="navbar-brand d-flex align-items-center">
                <img width="30px" height="30px" class="unifor-logo unifor-logo--negative" src="img/unifor-logo-vertical-negative.svg" alt="Logotipo da Unifor">
                <strong class="title-page">${name}</strong>
              </a>
              <nav class="st-navbar-desktop-menu float-left" role="navigation">
                <ul id="st-navbar-desktop-menu-nav" class="sf-menu">
                  <li><a href="https://unifor.br">Inicio</a></li>
                  <li><a href="#contact">Contato</a></li>
                </ul>
              </nav>
            </div>
          </div>
    `,
    place_img_right : ({img_src}) => `
        <div class="col-md-4 parent-center">
            <img width="250px" class="center" height="166,5px" src="${img_src}" alt="IMAGEM DO CENTRO DE CONVIVENCIA"/>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
              </p>
        </div>
    `,
    place_img_left : ({img_src}) => `
        <div class="row">
            <div class="col-md-8 parent-center">
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
                  </p>
            </div>
            <div class="col-md-4 parent-center">
                <img width="250px" class="center" height="166,5px" src="${img_src}" alt="IMAGEM DO CENTRO DE CONVIVENCIA"/>
            </div>
        </div>
    `,
    place_img_card : ({img_src, place_name = "Place"}) => `
        <div class="col-md-3" style="padding: 10px 10px 10px 10px">
            <div class="card card-shadow" style="width: 15rem; height: 100%">
                <img width="200px" height="130px" src="${img_src}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h4 class="card-title">${place_name}</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
            </div>
        </div>
        
    `,
    main_section : ({}) => `
        <div class="container-fluid splash" id="splash">
            <div class="container">
                <!--<img src="https://s-media-cache-ak0.pinimg.com/736x/71/9e/59/719e59481d2be40a77ab6c3386fc0a45&#45;&#45;photoshop-illustrator-illustrator-tutorials.jpg" alt="Portrait of Mr. Roboto" class="profile-image">-->
                <img width="200px" height="200px" class="unifor-logo unifor-logo--negative profile-image" src="https://www.unifor.br/o/unifor-theme/images/unifor-logo-vertical-negative.svg" alt="Logotipo da Unifor">
                <h1 style="font-family: Rockwell;">Bem- Vindo!</h1>
                <h1 class="intro-text"><span class="lead" id="typed">Nós Somos A </span></h1>
                <span class="continue"><a href="#about"><i class="fa fa-angle-down"></i></a></span>
            </div>
        </div>
    `,
    contact: ({}) => `
        <div class="container-fluid contact-me-container content-section" id="contact">
            <div class="container">
                <h1 class="intro-text text-center text-font-family" style="padding-bottom: 10px">Contact Me</h1>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <form-group-contact></form-group-contact>
                    </div>
        
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" placeholder="Message"></textarea>
                    </div>
                </div>
        
                <div class="text-center">
                    <button class="btn btn-default submit-button btn-lg btn-primary"><i class="fa fa-paper-plane"></i> Send</button>
                </div>
            </div>
        </div>
    `,
    form_input_field: ({id, icon, placeholder}) => `
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="${icon}"></i></span>
                </div>
                <input type="text" class="form-control" id="${id}" placeholder="${placeholder}" required="">
                <div class="invalid-feedback" style="width: 100%;">
                    Your ${id} is required.
                </div>
            </div>
        </div>
    `,
    footer : ({}) => `
    <div class="row">
        <div class="col-md-6 text-font-family" style="font-size: 44%">
            <p>© Copyright Phipper. All Rights Reserved</p>
            <p> Feito com <i class="fas fa-heart"></i>  por Tony Matheus Bernardo De Lima</p>
        </div>
        <div class="col-md-6 text-font-family" style="font-size: 44%">
            <p>Fundação Edson Queiroz - Universidade de Fortaleza</p>
            <p>Central de Atendimento: (85) 3477.3000 | Endereço: Av. Washington Soares, 1321- Edson Queiroz - CEP 60811-905 - Fortaleza-CE  Brasil</p>
        </div>
    </div>
    
    `
};

$.fn.componentHtml = function (which, attrs) {
    $(this).html(getComponent(which, attrs));
}

function getComponent(which, attrs){
    let html = ``;
    html = attrs.map(Components[which]).join('');
    return html
}